$(document).ready(function() { // create the auiselect2 boxes
    makeAuiSelect2("#id_name", "/phm/getprojects");
    $("#id_name").change(function(){
      $(this).closest("form").submit();
    });

    // javascript for filtering the list of projects
    $("#no-projects-message").hide();
    $("tr.project").hide();

    // create the javascript object to manage the filters
    var listProjectFilters = new Filters([
      {
          triggerId: "#show-favourites",
          classToChange: ".favourite"
      }, {
          triggerId: "#show-myprojects",
          classToChange: ".myproject"
      }, {
          triggerId: "#show-all",
          classToChange: "tr.project"
      }
    ]);

    var viewType = $(".project-view").text();
    listProjectFilters.shouldStoreLastViewedFilter(false);
    if (viewType == "mine") {
        var filter = listProjectFilters.getFilterWithTriggerId("#show-myprojects");
        listProjectFilters.displayFilter(filter);
    }
    else if (viewType == "all") {
        var filter = listProjectFilters.getFilterWithTriggerId("#show-all");
        listProjectFilters.displayFilter(filter);
    }
    else {
        listProjectFilters.shouldStoreLastViewedFilter(true);
    }
    listProjectFilters.setNothingToDisplayForFilterSelector("#no-projects-message");
    listProjectFilters.setFilterableElementsSelector("tr.project");
    listProjectFilters.start();
});
