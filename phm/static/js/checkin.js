var attributes = {0: 'FTO', 1: 'BT', 2:'SU', 3:'VM', 4:'EED', 5:'RM', 6:'D', 7:'V'};
var view;

$(document).ready(function(){
    var input = $(".checkin-item input");
    for (var i=0; i<input.length; i++) {
        if (input[i].type == "hidden") {
            if (input[i].id.split("-")[2] == "attribute"){
                input[i].value = attributes[input[i].id.split("-")[1]];
            }
            else if (input[i].id.split("-")[2] == "checkIn_id"){
                input[i].value = $(".checkin-container").attr("value");
            }
        }
    }

    var buttons_label = $('.health-buttons ul li label');
    var buttons_input = $('.health-buttons ul li label input');
    for (var i=0; i<buttons_input.length; i++) {
        buttons_input[i].nextSibling.data = '';
        if (buttons_input[i].value == 1){
            buttons_label[i].className = "red";
        }
        else if (buttons_input[i].value == 2){
            buttons_label[i].className = "yellow";
        }
        else if (buttons_input[i].value == 3){
            buttons_label[i].className = "green";
        }
        if(buttons_input[i].checked){
            check(buttons_input[i].id)
        }
    }

    $("body").on("click", ".health-buttons input", function(){
        var id = this.id;
        var split_id = id.split('_');
        var ul_id = split_id[0] + '_' + split_id[1];
        if ($(this).prop('checked') && $("label[for='"+id+"']").hasClass("selected")) {
            $(this).prop('checked', false);
            $(".health-buttons #"+ul_id+" li").children().removeClass("selected aui-icon aui-icon-small aui-iconfont-success");
        }
        else {
            check(id);
        }
    });

    $("body").on("click", "#checkin-clear-button", function(){
        $(".health-buttons li").children().removeClass("selected aui-icon aui-icon-small aui-iconfont-success");
    });

    if ($("#checkin-edit-button").length && window.location.search.indexOf("checkinId")>-1) {
        makeView();
        view = true;

        $("#checkin-edit-button").click(function(){
            toggleView();
        });
    } else {
        view = false;
        makeAuiSelect2('#id_project_id', '/phm/getprojects');
    }
});

var check = function(id) {
    var split_id = id.split('_');
    var ul_id = split_id[0] + '_' + split_id[1];
    $(".health-buttons #"+ul_id+" li").children().removeClass("selected aui-icon aui-icon-small aui-iconfont-success");
    $("label[for='"+id+"']").addClass("selected aui-icon aui-icon-small aui-iconfont-success");
};

var disableFields = function (isDisabled) {
    $(".disabled-with-border").attr("disabled", isDisabled);
    $(".disabled-without-border").attr("disabled", isDisabled);
    $(".health-buttons label").attr("disabled", isDisabled);
};

var makeView = function() {
    disableFields(true);
    $("#checkin-edit-button").attr("disabled", false);
    document.getElementById("checkin-edit-button").value = "Edit";
    if (document.getElementById("checkin-edit-button").className.indexOf("aui-button-primary") <0){
        document.getElementById("checkin-edit-button").className += " aui-button-primary";
    }

    $.each(attributes, function (i, val) {
        box = $("." + val)[0];
        text = $("." + val + " textarea")[0];

        colourElement = $("." + val + " input:checked")[0].parentElement;
        colour = window.getComputedStyle(colourElement).getPropertyValue("background-color");
        //colour = window.getComputedStyle(colourElement).getPropertyValue("background-color");

        box.style.boxShadow = colour + " 1px 1px 1px 1px";
        box.style.border = "solid " + colour;
        box.style.borderWidth = "1px 1px";
        //colour = colour.slice(0,3) + "a" + colour.slice(3);
        //colour = colour.slice(0,colour.length-1) + ", 0.15)";
        //console.log(colour)
        //box.style.backgroundColor = colour;
        text.style.backgroundColor = "rgba(0,0,0,0)";//fully transparent so you can see the div colour

    });
};

var makeEdit = function() {
    disableFields(false);
    document.getElementById("checkin-edit-button").value = "Cancel";
    document.getElementById("checkin-edit-button").className = document.getElementById("checkin-edit-button").className.replace("aui-button-primary","");
    $(".buttons-container").show();
    makeAuiSelect2('#id_project_id', '/phm/getprojects');
    $.each(attributes, function (i, val) {
        box = $("." + val)[0];
        text = $("." + val + " textarea")[0];
        box.style.backgroundColor = "";
        text.style.backgroundColor = "";
        box.style.boxShadow = "";
        box.style.borderWidth = "";
        box.style.border = "";
    });
};

var toggleView = function() {
    if (view) {
        makeEdit();
    } else {
        location.reload();
    }
    view = !view;
};