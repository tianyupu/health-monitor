var Filters = function(filters) {
  return _.extend({
    localStorageKey: "lastViewedFilter",
    storeLastViewed: false,
    nothingToDisplayForFilterSelector: undefined,
    allFilterableElementsSelector: undefined,
    filters: filters,
    defaultFilter: undefined,

    shouldStoreLastViewedFilter: function(flag) {
      if (typeof flag === "boolean") {
        this.storeLastViewed = flag;
      }
    },

    isStoringLastViewedFilter: function() {
      return this.storeLastViewed;
    },

    setLocalStorageKey: function(key) {
      if (typeof key === "string") {
        this.localStorageKey = key;
      }
    },

    getLocalStorageKey: function() {
      return this.localStorageKey;
    },

    setNothingToDisplayForFilterSelector: function(cssSelector) {
      if (typeof cssSelector === "string") {
        this.nothingToDisplayForFilterSelector = cssSelector;
      }
    },

    getNothingToDisplayForFilterSelector: function() {
      return this.nothingToDisplayForFilterSelector;
    },

    setFilterableElementsSelector: function(cssSelector) {
      if (typeof cssSelector === "string") {
        this.allFilterableElementsSelector = cssSelector;
      }
    },

    getFilterableElementsSelector: function() {
      return this.allFilterableElementsSelector;
    },

    setDefaultFilter: function(filter) {
      this.defaultFilter = filter;
    },

    getDefaultFilter: function(filter) {
      return this.defaultFilter;
    },

    getFilterWithTriggerId: function(triggerId) {
      return _.find(this.filters, function(filter) {
        return filter.triggerId === triggerId;
      });
    },

    saveLastViewedFilter: function(triggerId) {
      if (this.storeLastViewed && supportsLocalStorage()) {
        var filter = this.getFilterWithTriggerId(triggerId);
        if (filter) {
          window.localStorage[this.localStorageKey] = JSON.stringify(filter);
        }
      }
    },

    getLastViewedFilter: function() {
      if (this.storeLastViewed && supportsLocalStorage()) {
        var lastFilter = window.localStorage[this.localStorageKey];
        if (lastFilter) {
          return JSON.parse(lastFilter);
        }
      }
      return this.defaultFilter;
    },

    displayLastViewedFilter: function() {
      var lastViewed = this.getLastViewedFilter();
      if (lastViewed) {
        this.displayFilter(lastViewed);
      }
    },

    displayFilter: function(filter) {
      var classToShow = filter.classToChange;
      this.showProjectsWithClass(classToShow);
      this.updateCurrentSelectedFilter(filter);
    },

    registerClickEventsForFilters: function() {
      var self = this;
      _.each(this.filters, function(filter) {
        var id = filter.triggerId;
        $(id).click({
          filter: filter
        }, function(e) {
          self.displayFilter(e.data.filter);
          self.saveLastViewedFilter(e.data.filter.triggerId);

          e.preventDefault(); // stop the page from moving to top
        });
      });
    },

    updateCurrentSelectedFilter: function(filter) {
      $("[selected]").removeAttr("selected");
      var el = $(filter.triggerId);
      $(el).attr("selected", "");
    },

    showProjectsWithClass: function(cssSelector) {
      // hide everything initially
      $(this.nothingToDisplayForFilterSelector).hide();
      $(this.allFilterableElementsSelector).hide();

      // show only what matches the criteria
      var itemsToShow = $(cssSelector);
      if (itemsToShow.length == 0) { // if there's nothing to show, display the element that contains a "nothing for this filter" to this message
        $(this.nothingToDisplayForFilterSelector).fadeIn();
        this.trigger("nothingToDisplay");
      } else {
        itemsToShow.each(function(index, element) { // else show each of the applicable projects
          $(element).fadeIn();
        });
        this.trigger("displayedItems");
      }
    },

    start: function() {
      this.registerClickEventsForFilters();
      this.displayLastViewedFilter();
    },

    reset: function() {
      this.displayFilter(this.defaultFilter);
    }
  }, Backbone.Events);
};
