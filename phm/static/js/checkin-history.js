// All javascript relevant to displaying the history of checkins
require(['aui/inline-dialog2']); // for the hover dialogs (attribute description and checkin comments)

$(document).ready(function() {
  var scrollToCurrent = function() {
    var container = $("#checkins-container");
    var target = $(".current-checkin");
    if (container.length > 0) { // only try to scroll if there are health checks present on the screen
      container.animate({
        scrollLeft: target.offset().left - container.offset().left + container.scrollLeft()
      }, 1200);
    }
  };

  scrollToCurrent();

  $("#no-checkins-message").hide();

  // re-use our filters component to filter the checkins by stage (Envision, Create, etc);
  var defaultStageFilter = {
    triggerId: "#stage-all",
    classToChange: ".stage-1, .stage-2, .stage-3"
  };
  var stageFilters = new Filters([
    {
      triggerId: "#stage-1",
      classToChange: ".stage-1"
    }, {
      triggerId: "#stage-2",
      classToChange: ".stage-2"
    }, {
      triggerId: "#stage-3",
      classToChange: ".stage-3"
    },
    defaultStageFilter
  ]);
  stageFilters.setFilterableElementsSelector(".stage-1, .stage-2, .stage-3");
  stageFilters.setNothingToDisplayForFilterSelector("#no-checkins-message");
  stageFilters.setDefaultFilter(defaultStageFilter);
  stageFilters.on("nothingToDisplay", function() {
    $("#attributes-container").fadeOut();
  });
  stageFilters.on("displayedItems", function() {
    $("#attributes-container").fadeIn();
  });
  stageFilters.start();

  $("#scroll-to-current").click(function(e) {
    e.preventDefault();

    stageFilters.reset();
    scrollToCurrent();
  });
});
