$(document).ready(function() { // create the auiselect2 boxes
    makeAuiSelect2('#id_ownerInput', '/phm/staff');
    makeAuiSelect2('#id_memberInput', '/phm/staff');
    makeAuiSelect2('#id_sponsorInput', '/phm/staff');
    makeAuiSelect2('#id_department', '/phm/departments');

    // Stop form submission on press of enter button
    $('#id_name,#id_memberInput').keyup(function(e){
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    var members = $('.project-member');
    for(var x=0; x<members.length; x++) {
        add_member(members[x].innerHTML, "id_members");
    }

    /*
     * Add existing owner, sponsor and members to the form
     */
    if ($("#id_ownerInput").val() != "") {
        add_member($("#id_ownerInput").val(), "ownerSelect_id");
    }
    if ($("#id_sponsorInput").val() != "") {
        add_member($("#id_sponsorInput").val(), "sponsorSelect_id");
    }

    if ($("#id_members option").length != 0) {
        var existing = $("#id_members option");
        for (var i=0;i<existing.length;i++) {
            var username = existing[i].value;
            add_member(username, "id_members");
        }
    }

    /*
     * Detect addition of owner, sponsor and members to the form
     */
    $('#id_ownerInput').change(function(){
        var username = $("#id_ownerInput").val();
        add_member(username, "ownerSelect_id");

        resetInputField("id_ownerInput");
    });

    $('#id_sponsorInput').change(function(){
        var username = $("#id_sponsorInput").val();
        add_member(username, "sponsorSelect_id");

        resetInputField("id_sponsorInput");
    });

    $('#id_memberInput').change(function(){
        var username = $("#id_memberInput").val();
        add_member(username, "id_members");

        resetInputField("id_memberInput");
    });
});

/**
 * Add a member to the form
 **/
var add_member = function(username, id) {
    var idString = "#" + id + " option";
    var exists = false;
    var existing = $(idString);
    var avatarDiv;
    for (var i=0;i<existing.length;i++) {
        if (existing[i].value == username){
            exists = true;
            break;
        }
    }

    if (!exists) {
        // If owner or sponsor fields already contain a member
        if (id != "id_members") {
            if (existing) {
                if (id == "ownerSelect_id") {
                    avatarDiv = $('#ownerInput_id');
                }
                else {
                    avatarDiv = $('#sponsorInput_id');
                }
                // Remove existing fields
                existing.remove();
                avatarDiv.empty();
            }
        }
        create_profile_box(username, id);
        var option = document.createElement("option");
        option.value = username;
        option.selected = true;
        document.getElementById(id).appendChild(option);
    }
};

/**
 * Create profile box for user
 **/
function create_profile_box(username, id) {
    var idString = "#" + id + " option";
    var list;
    var resetId;
    // Get the member list and create a new entry
    if (id == "id_members") {
        list = document.getElementById('memberList');
        resetId = "id_memberInput";
    }
    else if (id == "ownerSelect_id") {
        list = document.getElementById('ownerInput_id');
        resetId = "id_ownerInput";
    }
    else if (id == "sponsorSelect_id") {
        list = document.getElementById('sponsorInput_id');
        resetId = "id_sponsorInput";
    }
    var entry = document.createElement("li");

    if (id != "id_members" && list.childElementCount < 1 || id == "id_members") {
        // Make the node to add to the list
        var node = document.createElement("div");
        getAvatarUrl(username, function (data) {
            data = JSON.parse(data);
            node.innerHTML = '<div class="remove-button"><span class="aui-icon aui-icon-small aui-iconfont-remove-label"></span></div><img src="' + data.url + '"><p>' + data.name + '</p>' + '<span class="username" value="' + data.username + '"></span>';
            // Remove users from the list if remove-button is clicked
            $(".remove-button").click(function () {
                this.parentElement.parentElement.parentNode.removeChild(this.parentElement.parentElement);
                var existing_options = $(idString);
                var select = document.getElementById(id);
                for (var i = 0; i < existing_options.length; i++) {
                    if (existing_options[i].value == username) {
                        select.remove(existing_options[i]);
                    }
                }
                resetInputField(resetId);
            });
        });

        // Add the new data to the list
        entry.appendChild(node);
        list.appendChild(entry);
    }
}

function resetInputField(id) {
    // Reset the field
    document.getElementById(id).value = "";
    makeAuiSelect2("#" + id, '/phm/staff');
}