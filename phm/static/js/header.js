$(document).ready(function() { // display the currently logged in user's avatar
  getAvatarUrl($("#username").attr("value"), function(data) {
    data = JSON.parse(data);
    $("#active-user-avatar").attr("src", data.url);
  });

  // load the list of favourite projects via AJAX
  $("#projects-tab").click(function(e) {
    // if the click doesn't expand the menu, don't do anything
    if ($("#projects-tab").attr("aria-expanded") === "false") {
      return;
    }
    // if the container to display the favourites list isn't even showing, don't do anything
    if ($("#favourite-projects-container").length == 0) { 
      return;
    }
    $.ajax({
      type: "GET",
      url: "/phm/favourites"
    }).done(function(response) {
      $("#favourite-projects-container").html(response);
    });
  });
});
