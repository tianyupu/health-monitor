// Required resources
require(['aui/form-validation'], function (validator) {
      AJS.validator = validator
      validator.validate(document.querySelector('input'));
});
require(['aui/form-notification']);

// responsible for creating the special select fields for Owner and Department
// pass in elementId as a string, eg. '#textbox'
// pass in ajaxUrl as the url of where to get the data, eg '/phm/staff'
var makeAuiSelect2 = function(elementId, ajaxUrl) {
  AJS.$(elementId).auiSelect2({
    multiple: false,
    minimumInputLength: 2,
    initSelection : function (element, callback) {
        var data = {id: elementId, text: $(elementId).val()};
        callback(data);
    },
    ajax: {
      url: ajaxUrl,
      type: 'GET',
      dataType: 'json',

      data: function(term) {
        return { q: term };
      },
      results: function(data) {
        return {
          results: $.map(data, function(item) {
            return {
              // label and value are dependent on the response fields from the server
              text: item.label,
              id: item.value,
            }
          }),
        };
      },
    },
    formatNoMatches: function (data) {
      return 'No matches found';
    },
  });
};

// Get the avatar URL of the current user
var getAvatarUrl = function(username, callback){
  $.ajax({
    method: "GET",
    url: window.location.origin + "/phm/hashemail", // use /phm so we can display the user avatar in the nav bar
    data: {"username": username},
    success: callback
  });
};

// given a jQuery element, make the form show an error
function invalidate($field, msg) {
  document.getElementById($field.attr("id")).setAttribute("data-aui-notification-error", msg);
}

// Make an ajax call to the server to favourite or un-favourite a project
var favouritise = function(project_id, isFavourite) {
  $.ajax({
    type: "GET",
    url: "favourite",
    data: {"project_id": project_id, "isFavourite": isFavourite}
  }).done(function(response) {
    console.log(response);
  });
};

// Check if the browser supports local storage
var supportsLocalStorage = function () {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch(e) {
    return false;
  }
};
