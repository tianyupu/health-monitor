$(document).ready(function() { // create the auiselect2 boxes
  if (sponsor) {
    getAvatarUrl(sponsor, function (data) {
      data = JSON.parse(data);
      document.getElementById("sponsor-avatar").style.backgroundImage = 'url("' + data.url + '")';
    });
  }
  if (owner) {
    getAvatarUrl(owner, function (data) {
      data = JSON.parse(data);
      document.getElementById("owner-avatar").style.backgroundImage = 'url("' + data.url + '")';
    });
  }
  $(document).on("click", ".aui-iconfont-unstar", function() {
    document.getElementById("favourite").className = "aui-icon aui-icon-small aui-iconfont-star";
    favouritise(projectId, true);
  });
  $(document).on("click", ".aui-iconfont-star", function() {
    document.getElementById("favourite").className = "aui-icon aui-icon-small aui-iconfont-unstar";
    favouritise(projectId, false);
  });

  $(document).on("click", ".checkin-date-heading", function(e){
    window.location=e.toElement.href;
  });
});
