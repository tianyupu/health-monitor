from django.conf.urls import patterns, url

from phm import views

urlpatterns = patterns('',
  url(r'^$', views.index, name='index'),
  url(r'^addproject', views.addproject, name='addproject'),
  url(r'^allprojects', views.listallprojects, name='listallprojects'),
  url(r'^addcheckin', views.addcheckin, name='addcheckin'),
  url(r'^viewproject', views.listallprojects, name='project'),
  url(r'^editproject', views.editproject, name='editproject'),
  url(r'^project', views.project, name='project'),
  url(r'^auth', views.auth_screen, name='auth_screen'),
  url(r'^oauth2callback', views.oauth2callback, name='oauth2callback'),
  url(r'^logout$', views.phm_logout, name='phm_logout'),
  url(r'^checkin_past', views.checkin_past, name='checkin_past'),
  url(r'^about$', views.about, name='about'),
  url(r'^staff', views.get_staff, name='get_staff'),
  url(r'^getprojects', views.get_projects, name='get_projects'),
  url(r'^departments', views.get_departments, name='get_departments'),
  url(r'^hashemail', views.hash_email, name='hash_email'),
  url(r'^favourites', views.favourites, name='favourites'),
  url(r'^favourite', views.favourite, name='favourite')
)
