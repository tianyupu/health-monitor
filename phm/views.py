from collections import OrderedDict
import json
import hashlib

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.contrib.auth.models import User as authUser
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.http import urlquote
from django.core.exceptions import ObjectDoesNotExist
from django.forms.formsets import formset_factory
import requests

from phm.models import Project, CheckIn, CheckInEntry, Favourites, User, UsersPerProject, Department
from phm import models
from phm.forms import ProjectForm, CheckInForm, CheckInAttributeForm, GetProjectForm

attributes = [
    ('FTO', "Full-time owner",
     "There is one lead who is accountable for the result of this project. This needs to be someone whose time is at least 80% dedicated to it, and who can champion the mission inside and outside of the team."),
    ('BT', "Balanced team",
     "Roles and responsibilities are clear and agreed upon. The project has people with the right blend of skill set. Acknowledge that team members can change by stage."),
    ('SU', "Shared understanding",
     "The team has a common understanding of why they're here, the problem/need, are convinced about the idea, confident they have what they need, and trust each other."),
    ('VM', "Values and metrics",
     "It's clear what success means from a business- and user's perspective, and there is a unique value proposition in place for the target users and to the business. Success is defined, with a goal, and how it will be measured."),
    ('EED', "End to end demo",
     "Some sort of demonstration has been created and tested, that demonstrates why this problem needs to be solved, and demonstrates its value."),
    ('RM', "Readme",
     "The project can be summarised in a one pager and shared with anyone so that they understand the purpose of the project, and its value."),
    ('D', "Dependencies",
     "Clear understanding of complexity, infrastructure involved, risks, resources, effort, and timeline. Clear understanding of who we depend on, and who depends on us."),
    ('V', "Velocity",
     "The team is making incremental progress by shipping concrete iterations to stakeholders (and even better to production), learning along the way, and implementing lessons along the way, resulting in greater success."),
    ("O", "Overall health",
     "The overall health of the project. This is calculated automatically by taking the average health of the above 8 areas.")
]


def index(request):
    template = loader.get_template('index.html')
    favourites = []
    username = request.user.username
    favourites_class = ""
    if username:
        current_user = User.objects.get(username=username)
        favourites = get_favourite_projects_for_user(current_user)

    if len(favourites) == 1:
        favourites_class = "single-project"
    elif len(favourites) == 2:
        favourites_class = "double-project"
    elif len(favourites) > 2:
        favourites_class = "many-projects"
    context = RequestContext(request, {
        'favourites': favourites,
        'favourites_class': favourites_class
    })
    return HttpResponse(template.render(context))


def auth_screen(request):
  template = loader.get_template('authenticate.html')
  context = RequestContext(request, {
    'auth_endpoint': 'https://accounts.google.com/o/oauth2/auth?',
    'client_id': '309626051647-u5fqanpa7qfcir85mj2unsbgrds546ro.apps.googleusercontent.com',
    'redirect_uri': 'http://' + request.get_host() + '/phm/oauth2callback',
    'scope': 'profile%20email',
    'error': request.GET.get('error', None)  # render errors if they exist
  })
  return HttpResponse(template.render(context))


def validateproject(request, title, button_text, edit, project_edit):
    # Get the post data into the form object
    form = ProjectForm(request.POST)

    print(request.POST)

    members = request.POST.getlist("members")
    name = request.POST.get("name")
    if name:
        name = name.strip()

    owner_username = request.POST.get("ownerInput")
    sponsor_username = request.POST.get("sponsorInput")
    owner = None
    sponsor = None

    # Check that the members exist
    confirmed_members = []
    all_members = []

    try:
        owner = User.objects.get(username=owner_username)
        if owner_username not in all_members:
            all_members.append(owner_username)
    except ObjectDoesNotExist:
        pass

    try:
        sponsor = User.objects.get(username=sponsor_username)
        if sponsor_username not in all_members:
            all_members.append(sponsor_username)
    except ObjectDoesNotExist:
        pass

    for member in members:
        try:
            if member not in confirmed_members:
                confirmed_members.append(member)
            if member not in all_members:
                all_members.append(member)
        except ObjectDoesNotExist:
            pass

    if len(confirmed_members) > 0 and 'members' in form.errors:
        del form.errors['members']

    exists = False
    # Check that the project name is unique
    try:
        existingProjects = Project.objects.filter(name=name)
        if len(existingProjects) and not(project_edit and project_edit.name == name):
            exists = True
    except ObjectDoesNotExist:
        pass

    if not exists and 'name' in form.errors: # If the project name is unique, but there is an error in the field, remove it
        del form.errors['name']
    elif exists: # If the project name is not unique, add an error to the form
        form.errors['name'] = "A project with that name already exists"

    department = None
    department_name = request.POST.get("department")
    if department_name:
        try:
            department = Department.objects.get(name=department_name)
        except ObjectDoesNotExist:
            pass

    # If there is an error, or there should be an error, return the errors
    if not sponsor or not owner or not department or 'name' in form.errors:
        if not sponsor:
            form.errors['sponsorInput'] = "Please select a sponsor"
        if not owner:
            form.errors['ownerInput'] = "Please select an owner"
        if not department:
            form.errors['department'] = 'Please select a department'
        else:
            form.fields["department"].initial = department.name
            form.fields["description"].initial = request.POST.get("description")
        return render(request, 'project-form.html',
                      {'form': form,
                       'title': title,
                       'buttonText': button_text,
                       'members': confirmed_members,
                       'name': name,
                       'ownerInput': owner_username,
                       'sponsorInput': sponsor_username,
                       'department': department_name,
                       'description': request.POST.get("description")})
    else:
        if not edit:
            # Create the project
            proj = Project(name=name,
                           link = request.POST.get("link"),
                           description=request.POST.get("description"),
                           owner=owner,
                           sponsor=sponsor,
                           department=department_name)
            proj.save()
            # Add the members to the project
            for member in all_members:
                try:
                    m = User.objects.get(username=member)
                    UsersPerProject(user_id=m, project_id=proj).save()
                except ObjectDoesNotExist:
                    pass
        else:
            project_edit.name = name
            project_edit.owner = owner
            project_edit.sponsor = sponsor
            project_edit.department = department_name
            project_edit.description = request.POST.get("description")
            project_edit.link = request.POST.get("link")

            current_members = get_members(project_edit)

            for current in current_members:
                if current not in all_members:
                    current.delete()

            for new in all_members:
                user = User.objects.get(username=new)
                try:
                    UsersPerProject.objects.get(user_id=user, project_id=project_edit)
                except ObjectDoesNotExist:
                    UsersPerProject(user_id=user, project_id=project_edit).save()

            project_edit.save()

        return HttpResponseRedirect('project?name=' + str(name))


@login_required
def addproject(request):
    title = "Create a project"
    button_text = "Create"
    if request.method == 'GET':
        form = ProjectForm()
        return render(request, 'project-form.html', {'form': form, 'title': title, 'buttonText': button_text})
    else:
        return validateproject(request, title, button_text, False, None)

@login_required
def editproject(request):
    title = "Edit project"
    button_text = "Update"
    try:
        project_name = request.GET['name']
        proj = Project.objects.get(name=project_name)
    except ObjectDoesNotExist:
        return HttpResponseRedirect('addproject')

    if request.method == 'GET':
        form = ProjectForm()

        current_members = []
        all_members = get_members(proj)
        for current in all_members:
            if current.user_id.fullName != proj.owner.fullName and current.user_id.fullName != proj.sponsor.fullName:
                current_members.append(current.user_id.username)

        print("current members ", current_members)
        form.initial = {'name': proj.name,
                        'ownerInput': proj.owner.username,
                        'sponsorInput': proj.sponsor.username,
                        'link': proj.link,
                        'department': proj.department,
                        'description': proj.description}
        return render(request, 'project-form.html', {'form': form,
                                                     'title': title,
                                                     'buttonText': button_text,
                                                     'members': current_members})
    else:
        return validateproject(request, title, button_text, True, proj)

@login_required
def listallprojects(request):
  # Just the form for now
  form = GetProjectForm()
  projects = Project.objects.all()
  username = request.user.username
  favourites = None
  userprojects = None
  viewtype = request.GET.get("view")
  if username:
    current_user = User.objects.get(username=username)
    favourites = get_favourite_projects_for_user(current_user)
    userprojects = get_projects_for_user(current_user)
  return render(request, 'list-all-projects.html', {
    'projects': projects,
    'form': form,
    'favourites': favourites,
    'userprojects': userprojects,
    'view_type': viewtype
  })

@login_required
def checkin_past(request):
  if request.method == 'GET':
    project_id = request.GET['project_id']
    try:
      project = Project.objects.get(id=project_id)  # Check that the id exists
    except ObjectDoesNotExist as e:
      print(e)
      return HttpResponseRedirect('addcheckin')
  checkin_list = CheckIn.objects.filter(project_id=project)
  checkin_list = checkin_list.extra(order_by=['-date'])
  checkins = OrderedDict()
  for ci in checkin_list:
    checkin_attrs = CheckInEntry.objects.filter(checkIn_id=ci)
    checkins[ci] = {}
    for attr in checkin_attrs:
      checkins[ci][attr.attribute] = attr
  context = {
    "attributes": attributes,
    "project": project,
    "checkins": checkins
  }
  r = render(request, 'checkin_past.html', context)
  return r

@login_required
def addcheckin(request):
  submit_text = "Create"
  title = "Create a health check"

  if request.method == 'GET':
    view_only = request.GET.get("viewOnly")
    CheckInAttributeFormSet = formset_factory(CheckInAttributeForm, extra=8)
    checkinEntryForm = CheckInAttributeFormSet()
    checkinForm = CheckInForm()
    # If this is an edit operation, then they will already have the checkin id
    checkinId = None
    try:
      checkinId = request.GET.get("checkinId")
      checkin = CheckIn.objects.get(id=checkinId)

      checkinForm.fields["project_id"].initial = checkin.project_id
      checkinForm.fields["stage"].initial = checkin.stage
      checkinForm.fields["status"].initial = checkin.status
      checkinForm.fields["comment"].initial = checkin.comment

      for formEntry, attribute in zip(checkinEntryForm, attributes):
        try:
          entry = CheckInEntry.objects.get(checkIn_id=checkin, attribute = attribute[0])
          formEntry.fields["checkIn_id"].initial = entry.id
          formEntry.fields["attribute"].initial = attribute
          formEntry.fields["colour"].initial = entry.colour
          formEntry.fields["comment"].initial = entry.comment
        except ObjectDoesNotExist:
          pass
      submit_text = "Update"
    except ObjectDoesNotExist:
      print("Could not get checkin with id", str(checkinId))
      checkin = None
    if request.GET.get("project"):
      proj_name = request.GET.get("project")
      try:
        print("Already have a project for it:", proj_name)
        project_id = Project.objects.get(name=proj_name)
        print("Got the project id")
        checkinForm.fields["project_id"].initial = project_id
      except ObjectDoesNotExist:
        pass
    
    if view_only:
      title = "Health check details"

    # Serve up the form
    return render(request, 'checkin.html', {'submit_text': submit_text,
                                             'view_only': view_only,
                                             'checkin_form': checkinForm,
                                             'formset': checkinEntryForm,
                                             'content': zip(checkinEntryForm, attributes),
                                             'checkIn_id': checkin,
                                             'title' : title})
  elif request.method == 'POST':
    data = request.POST.copy()
    user = User.objects.get(username=request.user.username)
    checkinId = request.GET.get("checkinId")
    userIsMember = True
    project = None

    # Validate the Checkin form
    try:
      project = Project.objects.get(name=data.get("project_id"))
      instance = CheckIn.objects.get(id=checkinId)
      checkinForm = CheckInForm(data, instance = instance)
    except Exception as e:
      print("Couldn't get initial checkin from" + str(checkinId))
      print(e)
      checkinForm = CheckInForm(data)

    if project:
      try:
        userIsMember = UsersPerProject.objects.get(project_id = project, user_id =user).id > -1
      except ObjectDoesNotExist:
        userIsMember = False

    if checkinForm.is_valid() and userIsMember:
      print("Saving checkin form cause its valid")
      checkin = checkinForm.save()
      print(checkin.id,checkinId,"<-- These should be the same, unless the second one is None")
    else:
      print("checkin form was not valid, or user was not a member")
      if not userIsMember:
        checkinForm.errors['project_id'] = ["You must be a member of this project to edit or create a checkin"]
      if not project:
        checkinForm.errors['project_id'] = ["Please select a project"]

      # If the checkin part of the form is not valid, then we can't really validate the rest of it properly
      CheckInAttributeFormSet = formset_factory(CheckInAttributeForm, extra=8)
      checkinEntryForm = CheckInAttributeFormSet(data)
      # In this case, we know that the checkin id is invalid (needs to be type checkIn), so delete the error as to not confuse people
      return render(request, 'checkin.html', {'submit_text': "Create",
                                         'checkin_form': checkinForm,
                                         'formset': checkinEntryForm,
                                         'content': zip(checkinEntryForm, attributes),
                                         'checkIn_id': None,
                                          'title' : title})

    # Validate the checkin entry formset
    # checkinEntries = CheckInEntry.objects.filter(Q(checkIn_id = checkin) & ~Q(attribute = "O"))
    # CheckInAttributeFormSet = formset_factory(CheckInAttributeForm, extra=8)
    # if checkinEntries:
    #   print("We are editing a bunch of checkin objects",checkinEntries)
    #   checkinEntryForm = CheckInAttributeFormSet(data, initial = checkinEntries)
    # else:
    #   checkinEntryForm = CheckInAttributeFormSet(data)
    overview_colour = 0.0
    total_entries = 0
    valid = True
    validated_checkin_entries = []
    for i in range(8):
      comment = data.get('form-' + str(i) + '-comment')
      attribute = data.get('form-' + str(i) + '-attribute')
      colour = data.get('form-' + str(i) + '-colour')

      if not colour:
        colour = 0
      else:
        total_entries += 1
      overview_colour += int(colour)

      if attribute and colour and checkin: # I.e, the form is valid
        try:
          entry = CheckInEntry.objects.get(checkIn_id = checkin, attribute = attribute)
          print("Just modifying existing checkin")
        except ObjectDoesNotExist:
          entry = CheckInEntry()
          print("Making a new checkin entry cause it doesn't exist yet")
        entry.checkIn_id = checkin
        entry.comment = comment
        entry.attribute = attribute
        entry.colour = colour
        validated_checkin_entries.append(entry)
        print("Added checkin entry for", checkin, attribute)
      else:
        valid = False

    if valid:
      # Save the overview object
      try:
        entry = CheckInEntry.objects.get(checkIn_id = checkin, attribute = "O")
      except ObjectDoesNotExist:
        entry = CheckInEntry()
      overview_colour = round(overview_colour / total_entries)
      colour_comment = {0: "Could not calculate the mean colour",
                        1: "The mean colour is red. Lots of areas will need work.",
                        2: "The mean colour is yellow. Some areas definitely need some work.",
                        3: "The mean colour is green. Clear skies for you :)"}
      entry.checkIn_id = checkin
      entry.comment = colour_comment[int(overview_colour)]
      entry.attribute = "O"
      entry.colour = overview_colour
      validated_checkin_entries.append(entry)
      for entry in validated_checkin_entries:
        entry.save()
      return redirect("/phm/project?name=" + checkin.project_id.name)
    else:
      # Form wont be able to be created with the checkin id being there (its the wrong object type)
      for i in range(8):
        del data["form-" + str(i) + "-checkIn_id"]

      CheckInAttributeFormSet = formset_factory(CheckInAttributeForm, extra=8)
      checkinEntryForm = CheckInAttributeFormSet(data)

      # We don't want the errors from the missing checkin id to be there
      for i in range(8):
        del checkinEntryForm.errors[i]['checkIn_id']

      if checkinEntryForm.is_valid():
        print("The entries form is valid")
        print("This shouldn't happen because we did manual validation. something is wrong")
        import pdb;pdb.set_trace()
        #checkinEntryForm.save()
        return redirect("/phm/project?name=" + checkin.project_id.name)
      else:
        print("The entries form is invalid")
        print("Errors:",checkinEntryForm.errors)
        return render(request, 'checkin.html', {'submit_text': "Create",
                                           'checkin_form': checkinForm,
                                           'formset': checkinEntryForm,
                                           'content': zip(checkinEntryForm, attributes),
                                           'checkIn_id': checkin.id,
                                           'title' : title})

def getProject(request, received):
  # used by the all projects page to go to a separate page to view a project's checkins
  template = loader.get_template('view-project.html')
  project = Project.objects.get(name=received)
  user = User.objects.get(username=request.user.username)
  isFavourite = len(Favourites.objects.filter(user_id=user, project_id=project)) > 0
  checkins = get_checkins_by_project(project, descending=False)
  checkins_by_attribute_and_stage = None
  stage_lengths = None
  stage_lengths_with_current = None
  if checkins:
    # these variables are passed into the context in order to render the fancy health bars
    checkins_by_attribute = checkins_grouped_by_attribute(
      checkins)  # groups checkins into attributes, as we render 1 row per attribute
    checkins_by_attribute_and_stage = OrderedDict()
    for attr, checkin in checkins_by_attribute.items():  # group each of these rows into stages, so we can distinguish between them
      entry_list = checkins_grouped_by_attribute_with_stages(checkin)
      checkins_with_correct_markers = mark_checkinentries_as_start_or_end_of_stages(entry_list)
      checkins_by_attribute_and_stage[
        attr] = checkins_with_correct_markers  # set the appropriate CSS classes so we can apply the right styles for visualising
    stage_lengths = get_length_of_stages_for_attribute(entry_list)
    stage_lengths_with_current = get_stage_length_with_current_checkin_separate(
      stage_lengths)  # provides the headings for the various stages, with the most recent checkin separate
  latest = get_latest_checkin(project)
  if latest and checkins:
    latestEntries = checkins.get(latest)
  else:
    latestEntries = None
  members = get_members(project)

  userInProject = False
  for member in members:
    if member.user_id.username == request.user.username:
      userInProject = True

  project_context = RequestContext(request, {
    'project': project,
    'checkins': checkins,
    'checkins_by_attribute': checkins_by_attribute_and_stage,
    'stage_lengths': stage_lengths,
    'stage_lengths_with_current': stage_lengths_with_current,
    'latest_checkin': latest,
    'latest_checkin_entries': latestEntries,
    'members': members,
    'isFavourite': isFavourite,
    'attributes': attributes,
    'userInProject': userInProject
  })

  return HttpResponse(template.render(project_context))


@login_required
def project(request):
  received = request.GET['name']
  return getProject(request, received)

def oauth2callback(request):
  if not request.GET:
    # Replace the '#' with a '?' so that the server can see the token,
    # otherwise anything after the '#' is not sent to the server.
    # Do this by injecting some javascript.
    # TODO: figure out a better way of doing this
    redirect_resp = '''
    <p>Redirecting...</p>
    <script>
      window.location.href = window.location.toString().replace('#', '?');
    </script>
    '''
    return HttpResponse(redirect_resp)
  else:
    error_redirect_url = '/phm/auth?error='
    if 'error' in request.GET:  # user pressed cancel in the consent screen, so we can't proceed
      return HttpResponseRedirect(
        error_redirect_url + urlquote('Could not authenticate you because you declined permission.'))
    token = request.GET['access_token']
    token_type = request.GET['token_type']
    expires = request.GET['expires_in']
    if not token_exists(token):
      add_token(token, token_type, expires)
    user_response = get_userinfo(token, token_type)
    if user_response.status_code == 200:  # successfully got the user info from the API call
      response_json = user_response.json()
      fullname = response_json['name']
      email = response_json['email']
      username, domain = email.split('@')
      if domain != 'atlassian.com':  # not an Atlassian user, so we deny their access
        return HttpResponseRedirect(
          error_redirect_url + urlquote('Access is only allowed for Atlassian employees.'))
      if not user_exists(email):  # if the user doesn't exist, create the django auth user *and* the phm user
        create_user(fullname, email, username)
      if not staff_exists(username):
        create_staff(fullname, username)
      user = authenticate(username=username,
                          password='')  # attempt to authenticate with the email retrieved from the API call
      if user is not None:
        if user.is_active:
          login(request, user)  # log the user into the current session
          return HttpResponseRedirect('/')
        else:
          return HttpResponseRedirect(error_redirect_url + urlquote(
            'Your credentials are correct, but your account has been disabled. Please contact your system administrator.'))
      else:
        return HttpResponseRedirect(
          error_redirect_url + urlquote('The username/password combination you have given is incorrect.'))

    return HttpResponseRedirect(error_redirect_url + urlquote(
      'There was a problem retrieving your details from Google. Please try again later.'))


@login_required
def phm_logout(request):
  logout(request)
  return HttpResponseRedirect('/')


@login_required
def get_staff(request):
  # used by the AUI select box to populate the dropdown of staff members
  queryval = request.GET['q']
  # search from the database
  serialisable_results = []
  # Based on full_name
  matching_names = models.Staff.objects.filter(full_name__icontains=queryval).filter(is_active=True)
  serialisable_results.extend([{'label': m.full_name, 'value': m.username} for m in matching_names if
                               {'label': m.full_name, 'value': m.username} not in serialisable_results])
  # Based on username
  matching_names = models.Staff.objects.filter(username__icontains=queryval).filter(is_active=True)
  serialisable_results.extend([{'label': m.full_name, 'value': m.username} for m in matching_names if
                               {'label': m.full_name, 'value': m.username} not in serialisable_results])

  return HttpResponse(json.dumps(serialisable_results))


@login_required
def get_projects(request):
  user = User.objects.get(username=request.user.username)
  # used by the AUI select box to populate the dropdown of staff members
  queryval = request.GET['q']
  # search from the database
  serialisable_results = []
  # Based on name
  matching_names = models.Project.objects.filter(name__icontains=queryval).filter()
  relavent_names = []
  for proj in matching_names: # Get all the projects that this user is a member of.
    try:
      upp = UsersPerProject.objects.get(project_id = proj, user_id = user) # Will fail if entry doesn't exist
      relavent_names.append(proj)
    except ObjectDoesNotExist:
      pass
  serialisable_results.extend([{'label': m.name, 'value': m.name} for m in relavent_names if
                               {'label': m.name, 'value': m.name} not in serialisable_results])
  # Based on description
  matching_names = models.Project.objects.filter(description__icontains=queryval).filter()
  relavent_names = []
  for proj in matching_names: # Get all the projects that this user is a member of.
    try:
      UsersPerProject.objects.get(project_id = proj, user_id = user) # Will fail if entry doesn't exist
      relavent_names.append(proj)
    except ObjectDoesNotExist:
      pass
  serialisable_results.extend([{'label': m.name, 'value': m.name} for m in relavent_names if
                               {'label': m.name, 'value': m.name} not in serialisable_results])

  return HttpResponse(json.dumps(serialisable_results))


@login_required
def get_departments(request):
  # used by the AUI select box to populate the dropdown of staff members
  queryval = request.GET['q']
  # search from the database
  matching_depts = models.Department.objects.filter(name__icontains=queryval).filter(is_active=True)
  serialisable_results = [{'label': m.name, 'value': m.name} for m in matching_depts]
  return HttpResponse(json.dumps(serialisable_results))


@login_required
def favourite(request):
  response = {}
  if request.method == "GET":
    project = models.Project.objects.get(id=request.GET["project_id"])
    user = User.objects.get(username=request.user.username)
    favourites = Favourites.objects.filter(user_id=user, project_id=project)

    if request.GET["isFavourite"] == "true" and len(favourites) == 0:
      fav = Favourites(user_id=user, project_id=project)
      fav.save()
      response["status"] = "Saved as a new favourite"
    elif request.GET["isFavourite"] == "false" and len(favourites) > 0:
      favourites.delete()
      response["status"] = "Deleted existing favourite"
    else:
      response["status"] = "State not changed"
  return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
def favourites(request):
  if request.method == "GET":
    current_user = User.objects.get(username=request.user.username)
    favourites = get_favourite_projects_for_user(current_user)
    return render(request, "nav-favourites-list.html", {
      "favourites": favourites
    })
  return HttpResponseBadRequest()


def get_members(project):
  return UsersPerProject.objects.filter(project_id=project)


#### THE NEXT FIVE FUNCTIONS ARE USED TO COMPUTE VARIOUS DATA STRUCTURES FOR DISPLAYING THE HISTORY VIEW OF CHECKINS ####
def checkins_grouped_by_attribute(
        checkins):  # checkins = {<CheckIn object>: {'attribute1': <CheckInEntry object>}, {'attribute2': <CheckInEntry object>}...}
  checkins_by_attribute = OrderedDict()
  for checkin, entries in checkins.items():
    for attribute, entry in entries.items():
      if attribute not in checkins_by_attribute:
        checkins_by_attribute[attribute] = []
      checkin_data = {'checkin': checkin}
      if entry:
        if entry.attribute == "O":
            comment = checkin.comment
        else:
            comment = entry.comment
        checkin_data.update({
          'colour': entry.colour,
          'comment': comment,
          'checkin_id': entry.checkIn_id_id
        })
      checkins_by_attribute[attribute].append(checkin_data)
  return checkins_by_attribute  # {'attribute1': [{'checkin': <CheckIn object>, 'colour': '1', 'comment': 'some comment'...}, {...}], 'attribute2': [..]}


def checkins_grouped_by_attribute_with_stages(
        entry_list):  # entry_list = [{'checkin': <CheckIn object>, 'colour': '1', 'comment': 'some comment'...}, {...}]
  entries_by_stage = []
  previous_entry = None
  current_stage_entries = []
  for entry in entry_list:
    checkin = entry.get('checkin')
    if previous_entry:
      previous_checkin = previous_entry.get('checkin')
      if previous_checkin.stage == checkin.stage:
        current_stage_entries.append(entry)
      else:
        entries_by_stage.append(current_stage_entries)
        current_stage_entries = [entry]
    else:
      current_stage_entries.append(entry)
    previous_entry = entry
  if current_stage_entries:
    entries_by_stage.append(current_stage_entries)
  return entries_by_stage  # entries_by_stage = [[{'checkin': <CheckIn object>, 'colour':..},{},...], [{},{},...]]


# From the list of lists returned by checkins_grouped_by_attribute_with_stages(), return a list of each of the lengths
# of the inner lists, along with the stage code (so we can get the name to display in the history view)
def get_length_of_stages_for_attribute(
        entries_by_stage):  # entries_by_stage = [[{'checkin': <checkin object>, 'colour':..},{},...], [{},{},...]]
  stage_codes_to_names = {
    1: "Envision it",
    2: "Make it",
    3: "Improve it"
  }
  stage_lengths = []
  for stage in entries_by_stage:
    if stage:
      first_entry = stage[0]
      stage_code = first_entry.get("checkin").stage
      stage_code_length_triple = (stage_code, stage_codes_to_names.get(stage_code), len(stage))
      stage_lengths.append(stage_code_length_triple)
  return stage_lengths  # stage_lengths = [(1, "Envision", 3), (2, "Create", 4), (3, "Improve", 1)]


def get_stage_length_with_current_checkin_separate(
        stage_lengths):  # stage_lengths = [(1, "Envision", 3), (2, "Create", 4), (3, "Improve", 1)]
  most_recent_stage_code, most_recent_stage_name, most_recent_stage_length = stage_lengths.pop()
  if most_recent_stage_length == 1:
    stage_lengths.append((most_recent_stage_code, "Current: " + most_recent_stage_name, most_recent_stage_length))
  else:
    stage_lengths.append((most_recent_stage_code, most_recent_stage_name, most_recent_stage_length - 1))
    stage_lengths.append((most_recent_stage_code, "Current: " + most_recent_stage_name, 1))
  return stage_lengths  # stage_lengths = [(1, "Envision", 3), ("Create", 4), ("Current: Improve", 1)]


# Mark entries as 'head', 'middle', 'tail' or 'solo', so we can render the correct lozenge in the front-end when
# visualising the history of checkins
def mark_checkinentries_as_start_or_end_of_stages(
        entries_by_stage):  # entries_by_stage = [[{'checkin': <checkin object>, 'colour':..},{},...], [{},{},...]]
  MARKER_NAME = 'marker'
  entry_list_with_marked_ends = []
  for stage in entries_by_stage:
    if len(stage) == 1:
      entry = stage[0]
      entry[MARKER_NAME] = 'solo'
      entry_list_with_marked_ends.append(entry)
    elif len(stage) > 1:
      first, last = stage[0], stage[-1]
      first[MARKER_NAME] = 'head'
      last[MARKER_NAME] = 'tail'
      for entry in stage:
        if not entry.get(MARKER_NAME):
          entry[MARKER_NAME] = 'middle'
        entry_list_with_marked_ends.append(entry)
  return entry_list_with_marked_ends  # entry_list_with_marked_ends = [{'checkin': <CheckIn object>, 'colour': '1', 'comment': 'some comment'..., 'marker': 'head'}, {...}]


#### END OF FUNCTIONS USED TO GENERATE HISTORY CHECKIN VIEW #####

def get_checkins_by_project(project, descending=True):
  if descending:
    descending_checkins_flag = '-date'
  else:
    descending_checkins_flag = 'date'
  checkin_list = CheckIn.objects.filter(project_id=project)
  checkin_list = checkin_list.extra(order_by=[descending_checkins_flag])
  if not (len(checkin_list)):
    return None
  checkins = OrderedDict()
  for checkin in checkin_list:
    entry_dict = OrderedDict([(a[0], None) for a in attributes])
    checkin_entries = CheckInEntry.objects.filter(checkIn_id=checkin)
    for e in checkin_entries:
      entry_dict[e.attribute] = e
    if len(checkin_entries) != 0:
      checkins[checkin] = entry_dict
  return checkins


def get_latest_checkin(project):
  checkin_list = CheckIn.objects.filter(project_id=project)
  checkin_list = checkin_list.extra(order_by=['-date'])
  for checkin in checkin_list:
    # if the user fills out the first check-in form but doesn't go on to populate the health checks,
    # we are covered because we only get the last checkin that actually had entries populated
    populated_checkins = CheckInEntry.objects.filter(checkIn_id_id=checkin.id)
    if populated_checkins:
      return checkin
  return None


def get_favourite_projects_for_user(user):
  return [Project.objects.get(id=fav.project_id.id) for fav in Favourites.objects.filter(user_id=user)]


def get_projects_for_user(user):
  return [upp.project_id for upp in UsersPerProject.objects.filter(user_id=user)]


def add_token(token, token_type, expires):
  t = models.Token(token=token, token_type=token_type, expires_in=expires)
  t.save()


def token_exists(token):
  try:
    existing_token = models.Token.objects.get(token=token)
  except ObjectDoesNotExist as err:
    return False
  else:
    return True


def get_userinfo(token, token_type):
  api_url = 'https://www.googleapis.com/userinfo/v2/me'
  headers = {'Authorization': '{} {}'.format(token_type, token)}
  r = requests.get(api_url, headers=headers)
  return r


def create_user(fullname, email, username):
  # Our user implementation
  u = User(fullName=fullname, username=username, email=email)
  u.save()

  # Django's built-in user for authentication
  # Use the email address as the username (for now)
  user = authUser.objects.create_user(username, email, '')
  user.save()


def create_staff(fullname, username):
  # User should be a subset of Staff. If its not, this needs to be fixed
  s = models.Staff(full_name=fullname, username=username, is_active=True)
  s.save()


def user_exists(email):
  try:
    existing_token = User.objects.get(email=email)
  except ObjectDoesNotExist as err:
    return False
  else:
    return True


def staff_exists(username):
  try:
    existing_token = models.Staff.objects.get(username=username)
  except ObjectDoesNotExist:
    return False
  return True


def about(request):
  template = loader.get_template('about.html')
  context = RequestContext(request, {
    "attribute_pairs": zip(attributes[:4], attributes[4:8]),
    "overview": attributes[8]
    })
  return HttpResponse(template.render(context))


def hash_email(request):
  try:
    staff = models.Staff.objects.get(username=request.GET['username'])
  except ObjectDoesNotExist:
    HttpResponse(json.dumps({'url': '', 'name': '', 'username': ''}))

  try:
    user = User.objects.get(username=request.GET['username'])
  except ObjectDoesNotExist:
    email = staff.username + "@atlassian.com"
    create_user(staff.full_name, email, staff.username)
    user = User.objects.get(username=staff.username)

  email = user.email.strip().lower()
  baseUrl = "https://avatar-cdn.atlassian.com/"
  emailHash = hashlib.md5()
  emailHash.update(email.encode('utf-8'))
  url = baseUrl + emailHash.hexdigest()

  return HttpResponse(json.dumps({'url': url, 'name': user.fullName, 'username': user.username}))
