from django.db import models

class User(models.Model):
    fullName = models.CharField(max_length=60, null=True)
    username = models.CharField(max_length=40, null=True, blank=True)
    email = models.CharField(max_length=60, null=True)

    def __str__(self):
        return self.fullName

class Department(models.Model):
    name = models.CharField(max_length=60, primary_key=True)
    is_active = models.BooleanField(default=True)

class Project(models.Model):
    name = models.CharField(max_length=30)
    link = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    owner = models.ForeignKey(User, related_name="owner")
    sponsor = models.ForeignKey(User, related_name="sponsor")
    nextCheckIn = models.DateTimeField(null=True, blank=True)
    department = models.CharField(max_length=60, null=True, blank=True)
    avatar = models.ImageField("Project Avatar", upload_to="images/", blank=True, null=True)

    def __str__(self):
        return self.name

class Favourites(models.Model):
    user_id = models.ForeignKey(User)
    project_id = models.ForeignKey(Project)

class CheckIn(models.Model):
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    project_id = models.ForeignKey(Project)
    stage = models.IntegerField(choices=(
        (1, 'Envision'),
        (2, 'Create'), 
        (3, 'Improve'),
        ), default = 1)
    status = models.CharField(max_length=50, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

class CheckInEntry(models.Model):
    checkIn_id = models.ForeignKey(CheckIn)
    attribute = models.CharField(choices=(
        ('FTO', 'Full Time Owner'), 
        ('BT', 'Balanced Team'), 
        ('SU', 'Shared Understanding'),
        ('VM', 'Value and Metrics'),
        ('EED', 'End-to-End Demo'),
        ('RM', 'Readme'),
        ('D', 'Dependencies'),
        ('V', 'Velocity'),
        ('O', 'Overview')
    ), max_length=3)
    colour = models.IntegerField(choices = (
        (0, 'grey'),
        (1, 'red'), 
        (2, 'yellow'), 
        (3, 'green'),
    ), default=0)
    comment = models.TextField(null=True, blank=True)

class UsersPerProject(models.Model):
    user_id = models.ForeignKey(User)
    project_id = models.ForeignKey(Project)

class Token(models.Model):
    token = models.TextField(null=False)
    token_type = models.CharField(max_length=50, null=False)
    expires_in = models.IntegerField()
    time_obtained = models.DateTimeField(auto_now_add=True)

class Staff(models.Model):
    username = models.CharField(max_length=40, null=True, blank=True)
    full_name = models.CharField(max_length=60, null=True)
    is_active = models.BooleanField(default=True)
