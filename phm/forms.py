from django import forms
from phm.models import Project, CheckIn, CheckInEntry
from django.core.exceptions import ObjectDoesNotExist

class GetProjectForm(forms.ModelForm):
    name = forms.CharField(
        label="Search projects",
        max_length=30,
        widget=forms.TextInput(attrs={
            "class": "text project-search-box",
            "placeholder": "Find a project by name"
        })
    )

    class Meta:
        model = Project
        fields = ['name']

class ProjectForm(forms.ModelForm):
    name = forms.CharField(
        label='Project Name:',
        max_length=30,
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-name', 'data-aui-validation-field': '', 'data-aui-validation-required': 'required', "data-aui-notification-field": ""})
    )
    link = forms.CharField(
        label='Project website',
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-link', 'data-aui-validation-field': '', 'data-aui-validation-required': 'required', "data-aui-notification-field": "", 'placeholder': 'Add a link to EAC'}),
    )
    department = forms.CharField(
        label='Department:',
        max_length=60,
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-department'}),
        required=False
    )
    description = forms.CharField(
        label='Description:',
        widget=forms.Textarea(attrs={'class': 'textarea', 'className': 'project-description', 'placeholder': 'Add a description', 'rows': 3}),
        required=False
    )
    ownerInput = forms.CharField(
        label='Project owner:',
        max_length=60,
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-owner', 'overrideField': True})
    )
    sponsorInput = forms.CharField(
        label='Project sponsor:',
        max_length=60,
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-sponsor', 'overrideField': True})
    )
    memberInput = forms.CharField(
        label='Additional members:',
        max_length=60,
        widget=forms.TextInput(attrs={'class': 'text', 'className': 'project-members'}),
        required=False
    )
    members = forms.MultipleChoiceField(
        label='Members',
        widget=forms.SelectMultiple(attrs={'className': 'project-members-list', 'overrideField': True}),
        required=False
    )

    class Meta:
        model = Project
        fields = ['name', 'department', 'description', 'ownerInput', 'sponsorInput', 'memberInput', 'members']

class CheckInForm(forms.ModelForm):
    project_id = forms.CharField(
        label='Project',
        max_length=30,
        widget=forms.TextInput(attrs={'class': 'text disabled-with-border'})
        )
    stage = forms.ChoiceField(
        label='Stage',
        choices=(
            (1, 'Envision'),
            (2, 'Create'),
            (3, 'Improve'),
        ), 
        widget=forms.Select(attrs={'class': 'select disabled-with-border'})
        )
    status = forms.CharField(
        label='Status',
        max_length=50,
        widget=forms.TextInput(attrs={'class': 'text disabled-with-border', 'placeholder': 'E.g. Active, Roadmapped'}),
        required=False
        )
    comment = forms.CharField(
        label='Comment',
        max_length=140,
        widget=forms.Textarea(attrs={
            'class': 'textarea medium-field disabled-with-border',
            'placeholder': 'Add a comment',
            'rows': 3}),
        required=False
        )

    def clean_project_id(self):
        proj_name = self.cleaned_data['project_id']
        try:
            project = Project.objects.get(name=proj_name)
            return project
        except ObjectDoesNotExist:
            raise forms.ValidationError("Project doesn't exist")

    class Meta:
        model = CheckIn
        fields = ['project_id', 'stage', 'status', 'comment']

class CheckInAttributeForm(forms.ModelForm):
    checkIn_id = forms.CharField(widget=forms.HiddenInput())
    attribute = forms.CharField(widget=forms.HiddenInput())
    colour = forms.ChoiceField(
        label="Health",
        choices=((1, 'Unhealthy'),
            (2, 'Moderate'),
            (3, 'Healthy')),
        widget=forms.RadioSelect(attrs={"class":"circle-radio disabled-without-border"}))
    comment = forms.CharField(
        label='Comment',
        max_length=140,
        widget=forms.Textarea(attrs={
            'class': 'textarea disabled-without-border',
            'placeholder': "Comment. Any Changes? What worked or didn't work",
            'rows': 4}),
        required=False
        )

    def clean_checkIn_id(self):
        checkin_id = self.cleaned_data['checkIn_id']
        try:
            id = int(checkin_id)
            checkin = CheckIn.objects.get(id=checkin_id)
            return checkin
        except (ObjectDoesNotExist, ValueError):
            raise forms.ValidationError("Check-in doesn't exist")

    class Meta:
        model = CheckInEntry
        fields = ['checkIn_id', 'attribute', 'colour', 'comment']


