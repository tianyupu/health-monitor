import phm.models as models

user = models.Staff()
user.username = "staff1"
user.full_name = "staff1"
user.is_active = True
user.save()

user = models.Staff()
user.username = "staff2"
user.full_name = "staff2"
user.is_active = True
user.save()

dept = models.Department()
dept.name = "department1"
dept.is_active = True
dept.save()

dept = models.Department()
dept.name = "department2"
dept.is_active = True
dept.save()
