#!/usr/bin/env bash

# Magical setup script for the Health Monitor app.
# For Mac OS X only.

# Install python3 if it doesn't exist
if [[ ! -e /usr/local/bin/python3 ]]; then
  echo "Installing python3..."
  brew install python3
fi

# Set up Django on python3
if [[ ! -e /usr/local/bin/pip3 ]]; then
  echo "Something is wrong with your pip... Exiting."
  exit 1
else
  echo "Installing python packages from requirements.txt..."
  pip3 install -r requirements.txt
fi

echo "Checking the installed django version..."
installed_django=`django-admin.py version`
if [[ $installed_django != 1.7.4 ]]; then
  echo "Something went wrong with installing django... Exiting."
  exit 2
else
  echo "Django version OK."
fi

if [[ ! -e /usr/local/bin/psql ]]; then
  echo "Installing postgres 9.4..."
  brew install postgres
fi

echo "Setting up your postgres for django..."
psql -c "CREATE DATABASE phm"
psql -d "phm" -c "CREATE USER test"
python3 manage.py makemigrations
python3 manage.py migrate

echo "Setup script completed successfully."
