# Project Health Monitor

[![Build Status](https://drone.io/bitbucket.org/tianyupu/health-monitor/status.png)](https://drone.io/bitbucket.org/tianyupu/health-monitor/latest)

For better viewing of project statuses, powered by Python 3.

Check out the [wiki](https://bitbucket.org/tianyupu/health-monitor/wiki) for more info!

Our backlog is on [jira](https://jdog.jira-dev.com/secure/RapidBoard.jspa?rapidView=687&projectKey=HMA&view=planning) 

## Live site
[Click here](http://project-health-monitor.herokuapp.com/)

## Setup instructions for Mac OS X
Clone this repository and run `setup.sh` in your favourite shell.

This will set up python3, pip3, django 1.7.4, psycopg2 and postgres on your
machine. A database called `phm` will be created, under the user `test`. The
final step does a django `migrate` which creates all the necessary tables
in the `phm` database.

It will also download requests, dj-database-url and dj-static for easier handling
of static files and database URLs in production.

## Running the local development server
After running `setup.sh` successfully, run
```shell
$ python3 manage.py runserver
```
to start a local development server which automatically reloads whenever any
source files change. Navigate to [localhost:8000](http://localhost:8000/) to
view the app.

## Troubleshooting
### setup.sh
Make sure that it is executable by typing `chmod +x setup.sh`.

### postgres
If you're getting postgres errors, make sure the postgres server is actually
running:
```shell
$ ps -elf | grep postgres | wc -l
6
```
This number should be greater than 1.

### python
We're using python 3+, so make sure you have python 3:
```shell
$ python3
Python 3.4.2 (default, Oct 19 2014, 17:55:38)
[GCC 4.2.1 Compatible Apple LLVM 6.0 (clang-600.0.54)] on darwin
...
```

pip3 should automatically exist if you have python3.

###   runserver
We're running Postgres on port 5432:
```
Is the server running on host "127.0.0.1" and accepting
TCP/IP connections on port 5432?
...
```

This means you haven't started the Postgres server, or haven't started it on the right port.
