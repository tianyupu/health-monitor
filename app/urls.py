from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

  url(r'^$', include('phm.urls')),
  url(
    r'^favicon.ico$',
    RedirectView.as_view(
      url='/static/images/favicon.ico',
      permanent=False),
    name="favicon"
  ),
  url(r'^phm/', include('phm.urls')),
  url(r'^admin/', include(admin.site.urls)),
)

handler404 = 'app.views.custom_page_not_found'
handler500 = 'app.views.custom_server_error'
