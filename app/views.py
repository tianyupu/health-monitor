from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader

def custom_page_not_found(request):
  template = loader.get_template('404.html')
  context = RequestContext(request)
  return HttpResponse(template.render(context))

def custom_server_error(request):
  template = loader.get_template('500.html')
  context = RequestContext(request)
  return HttpResponse(template.render(context))
