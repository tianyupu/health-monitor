"""
Django settings for app project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
INSECURE_SECRET_KEY = 'b9wld+dprye_gcy*bkj9_(@ulfu-2u%ksl27%@g9e3+8p!iwvm'
ENV_SECRET_KEY = os.environ.get('SECRET_KEY')
SECRET_KEY = ENV_SECRET_KEY if ENV_SECRET_KEY else INSECURE_SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
MODE = os.environ.get('MODE')
DEBUG = True if MODE != 'prod' else False

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['localhost:8000', 'project-health-monitor.herokuapp.com', '127.0.0.1:8000', 'localhost', '127.0.0.1'] # allowed all host headers according to https://devcenter.heroku.com/articles/getting-started-with-django


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'phm',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
import dj_database_url
DATABASES = {
    'default': dj_database_url.config(default='postgres://test:phmpassword@127.0.0.1:5432/phm')
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Australia/Sydney'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

BASE = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = 'phm/static'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
  os.path.join(BASE_DIR, 'static'),
)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
# from https://devcenter.heroku.com/articles/getting-started-with-django
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Default URL for logins
LOGIN_URL = '/phm/auth/'
